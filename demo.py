from printStatus import printStatus as ps
import time


def demo():
    ps.module("This module is named 'The Blue Sky'")
    
    ps.running("First task")
    time.sleep(1)
    ps.updateDone("First task")
    
    ps.running("Second task")
    time.sleep(1)
    ps.updateDone("Second task")
    
    ps.running("Third task")
    time.sleep(1)
    ps.updateWarning("Third task")
    
    ps.running("Fourth task")
    time.sleep(1)
    ps.updateDone("Fourth task")
    
    ps.running("Fifth task")
    time.sleep(1)
    ps.updateFailed("Fifth task")
    
    ps.running("Sixth task")
    time.sleep(1)
    ps.updateDone("Sixth task")
    
    ps.running("Doing an expensive computation")
    for i in range(10): 
        ps.progressBar(i, 10)
        time.sleep(1)
    ps.updateDone("Doing an expensive computation", progressbar=True)
    
    ps.done("This is the end of this demo script!")



if __name__ == '__main__':
    while True:
        demo()


